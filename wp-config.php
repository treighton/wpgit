<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
// ** MySQL settings - You can get this info from your web host ** //
// Check for a local config file
if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
    define( 'WP_LOCAL_DEV', true );
    include( dirname( __FILE__ ) . '/wp-config-local.php' );
} else {
	// LIVE DB
	// define('DB_NAME', 'database');
	// define('DB_USER', 'user');
	// define('DB_PASSWORD', 'password');
	// define('DB_HOST', 'localhost');
}



/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'E/`Vt)R7HGZ-`]wwB}y!6CRuv>-_j&=q0][:-[,}}`7#/A?h(88,#rmettJ>Bw73');
define('SECURE_AUTH_KEY',  'Lj?*S3}p(pvj&7;n)~Bp:-9dRN3vafso>{ll+VQ/6,/y,;?ui)JRZ2!?^5V]2rb3');
define('LOGGED_IN_KEY',    '6[E+jch*pw:f}W/TRFxM:qxxH:k)n5DRhg4sh`(4LKT0ny%Wo9xo<vR7 wmH.eL ');
define('NONCE_KEY',        '20eHiz>U`p-(:`P3uJFG;M11n:-j|MyW+tDv1hV]{D(PxdePnYigMC6Wfx:sMs,y');
define('AUTH_SALT',        '.F&Xx.$pZ]v7}|*aDb5vMw@IY^1B{!6if!}tEVc!L96:tp!M,.9i;xUeERd*}lS`');
define('SECURE_AUTH_SALT', 'AF>&y*J,4XXSw9fySBOJIW&YA1O{t[cpSKd~$WHDwIX_J[1xeA%.]@O)N}5J5 =r');
define('LOGGED_IN_SALT',   '9(rSw=hv.DOwqOV]zBUKvZmPxIWC@}b,)lr&WwBQXQ:b*$L4xfPl/!wC(rBEPxc3');
define('NONCE_SALT',       'iuS50zclmCw+bR|yWGLtP #;:Ec8tfai[8FLM4Q.,4t[YfJ@]AVTrez#i-eMLa<i');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'starter_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
